<?php

interface Data{
    public function phone(string $phone);
    public function name(string $name);
    public function surname(string $surname);
    public function email(string $email);
    public function address(string $address);
}

class Contact implements Data{
    protected $contact;

    public function phone(string $phone)
    {
        $this->contact->phone = $phone;
        return $this;
    }

    public function name(string $name)
    {
        $this->contact->name = $name;
        return $this;
    }

    public function surname(string $surname)
    {
        $this->contact->surname = $surname;
        return $this;
    }

    public function email(string $email)
    {
        $this->contact->email = $email;
        return $this;
    }

    public function address(string $address)
    {
        $this->contact->address = $address;
        return $this;
    }

    public function build(){
        return $this;
    }
}

$contact = new Contact();
$newContact = $contact->phone('000-555-000')
    ->name("Johns")
    ->surname("Surname")
    ->email("john@email.com")
    ->address("Some address")
    ->build();
var_dump($newContact);