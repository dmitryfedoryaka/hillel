<?php
abstract class Factory{
    abstract public function PaymentMethod() : Payment;
    public function operation(){
        /*
         * Here we will use $this->PaymentMethod()
         */
    }
}

class Product1 extends Factory{
    public function PaymentMethod(){
        return new Visa;
    }
}

class Product2 extends Factory{
    public function PaymentMethod(){
        return new MasterCard;
    }
}

interface Payment{
    public function pay();
}

class Visa implements Payment{
    public function pay(){
        ...
    };
}

class MasterCard implements Payment{
    public function pay(){
        ...
    };
}