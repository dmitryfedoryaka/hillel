<?php
$host = 'localhost';
$db   = 'hillel';
$user = 'root';
$pass = '123';
$charset = 'utf8';

define('SQL_PATH', __DIR__ . '/sql/');

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
$pdo = new PDO($dsn, $user, $pass, $opt);
try{
    switch ($_GET['par']) {
        case('createform') :
            {
                echo "<form action='' method='GET'><input type='hidden' name='par' value='create' /><button type='submit'>Create table</button></form>";
            }
            break;
        case('create') :
            {
                $query = file_get_contents(SQL_PATH . 'create.sql');
                $query = $pdo->prepare($query);
                $query->execute();
                echo 'Database was created';
            }
            break;
        case('insertform') : {
                echo "<form action='' method='GET'>
                <input type='hidden' name='par' value='insert' />
                <input placrholder='Your name' type='text' name='name'/>
                <input placeholder='Your surname' type='text' name='surname'/>
                <input placeholder='Your age' type='text' name='age'/>
                <input placeholder='Your email' type='text' name='email'/>
                <button type='submit'>Create table</button></form>";
            }
            break;
        case('insert') :
            {

                $users = [
                    'name' => $_GET['name'],
                    'surname' => $_GET['surname'],
                    'age' => $_GET['age'],
                    'email' => $_GET['email']
                ];

                $query = 'INSERT INTO users (name,surname,age,email) VALUES (:name, :surname, :age, :email)';
                $query = $pdo->prepare($query);
                $query->execute($users);
                echo 'Data was inserted';
            }
            break;
        case('selectform') :
            {
                $query = 'SELECT id FROM users';
                $query = $pdo->prepare($query);
                $query->execute();
                $results = $query->fetchALL();
                echo "<form action='' method='GET'>";
                echo "<input type='hidden' name='par' value='select' />";
                echo "<select name='id'>";
                foreach($results as $result){
                    echo "<option value='" . $result['id'] . "'>" . $result['id'] . "<option>";
                }
                echo "</select>";
                echo "<button type='submit'>Show info</button>";
                echo "</form>";
            }
            break;
        case('select') :
            {
                $query = 'SELECT * FROM users WHERE id=' . $_GET['id'];
                $query = $pdo->prepare($query);
                $query->execute();
                $results = $query->fetchALL();
                foreach($results as $result){
                    echo $result['id'] . "<br/>";
                    echo $result['name'] . "<br/>";
                    echo $result['surname'] . "<br/>";
                    echo $result['age'] . "<br/>";
                    echo $result['email'] . "<br/>";
                }
            }
            break;
        case('deleteform') :
            {
                $query = 'SELECT id FROM users';
                $query = $pdo->prepare($query);
                $query->execute();
                $results = $query->fetchALL();
                echo "<form action='' method='GET'>";
                echo "<input type='hidden' name='par' value='delete' />";
                echo "<select multiple name='id'>";
                for($i = 0; $i < count($results); $i++){
                    echo "<option value='" . $results[$i]['id'] . "'>" . $results[$i]['id'] . "<option>";
                }
                echo "</select>";
                echo "<button type='submit'>Delete info</button>";
                echo "</form>";
            }
            break;
        case('delete') :
            {
                $gets = explode('?', $_SERVER['REQUEST_URI']);
                $ids = substr($gets[1],11);
                $where = str_replace('&', ' OR ', $ids);
                $ids = str_replace('ids', 'id');
                $query = 'DELETE FROM users WHERE ' . $where;
                $query = $pdo->prepare($query);
                $query->execute();
                echo "Ids were deleted";
            }
            break;
        default :
        {
            die('test');
        }
    }
}catch (PDOException $e) {
    switch ($_GET['par']) {
        case('create') :
            {
                echo "Database is already exists";
            }
            break;
        default :
        {
            die('test2');
        }
    }
}