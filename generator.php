<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function ArraysGenerator($file){
    $f = fopen($file,'r');
    while($line = fgets($f)){
        yield explode(',',$line);
    }
    fclose($f);
}

foreach (ArraysGenerator('cats.csv') as $line) {
    echo "<pre>";
    print_r($line);
    echo "</pre>";
}