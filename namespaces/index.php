<?php

spl_autoload_register(function ($namespace) {
    include_once(__DIR__ . '/' . str_replace('\\', '/', $namespace) . '.php');
});

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\OrdersController;
use App\Http\Controllers\MainController;
use App\Http\Helpers\ImageHelper;
use App\Models\Order;
use App\Models\User;
use App\Models\Product;

new DashboardController('DashboardController');
echo '<br/>';
new OrdersController('OrdersController');
echo '<br/>';
new MainController('MainController');
echo '<br/>';
new ImageHelper('ImageHelper');
echo '<br/>';
new Order('Order');
echo '<br/>';
new User('User');
echo '<br/>';
new Product('Product');
echo '<br/>';
if (class_exists('test', $autoload = true)) {
    $domain = new test();
} else {
    echo 'File not found';
}
