<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

class Logger
{

    private $format;
    private $delivery;
    private $string;

    public function setString($string)
    {
        $this->string = $string;
    }

    public function getString()
    {
        return $this->string;
    }

    public function setFormat(Format $format){
        $this->format = $format;
    }

    public function setDelivery(Delivery $delivery){
        $this->delivery = $delivery;
    }

    public function log()
    {
        $this->deliver($this->format);
    }

    public function deliver($format)
    {
        return $this->delivery->getDelivery($format,$this);
    }
}

interface Format{
    public function getFormat(Logger $logger);
}

class Raw implements Format{
    public function getFormat(Logger $logger)
    {
        return $logger->getString();
    }
}

class WithDate implements Format{
    public function getFormat(Logger $logger)
    {
        return date('Y-m-d H:i:s') . $logger->getString();
    }
}

class WithDateAndDetails implements Format{
    public function getFormat(Logger $logger)
    {
        return date('Y-m-d H:i:s') . $logger->getString() . ' - With some details';
    }
}

interface Delivery{
    public function getDelivery(Format $format,Logger $logger);
}

class ByEmail implements Delivery{
    public function getDelivery(Format $format, Logger $logger){
        echo "Вывод формата (" . $format->getFormat($logger) . ") по имейл";
    }
}

class BySMS implements Delivery{
    public function getDelivery(Format $format,Logger $logger){
        echo "Вывод формата (" . $format->getFormat($logger) . ") в смс";
    }
}

class ToConsole implements Delivery{
    public function getDelivery(Format $format, Logger $logger){
        echo "Вывод формата (" . $format->getFormat($logger) . ") в смс";
    }
}

$logger = new Logger();
$logger->setString('Emergency error! Please fix me!');
$logger->setFormat(new WithDate);
$logger->setDelivery(new BySMS);
$logger->log();