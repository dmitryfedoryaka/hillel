<?php

class Users{
    private $name;
    private $age;
    private $email;

    public function setName($name){
        $this->name = $name;
    }

    public function setAge($age){
        $this->age = $age;
    }

    public function getAll(){
        return $this->age . "|" . $this->name;
    }

    public function __call($name,$arguments){
        echo "Метода " . $name . " не существует<br/>";
    }
}

$user = new Users();
$user->setName('Dmitry');
$user->setAge(36);
$user->setEmail('test@test.com');
echo $user->getAll();