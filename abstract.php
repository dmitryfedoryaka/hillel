<?php

abstract class User{
    abstract function increaseRevenue();
}

class Employee extends User{
    private $workstatus;
    private $salary;
    public function __construct($workstatus,$salary){
        $this->workstatus = $workstatus;
        $this->salary = $salary;
    }

    public function increaseRevenue()
    {
        if($this->workstatus == 'good'){
            $this->salary += 1000;
            return $this->salary;
        }
    }
}

class Student extends User{
    private $mark;
    private $salary;
    public function __construct($mark,$salary){
        $this->mark = $mark;
        $this->salary = $salary;
    }

    public function increaseRevenue()
    {
        if($this->mark == 5){
            $this->salary += 1000;
            return $this->salary;
        }
    }
}