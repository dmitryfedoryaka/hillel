<?php

class SecondWorker{
    public $name;
    public $age;
    public $salary;

    public function __construct($name,$age,$salary){
        if($age < 18) {
            return;
        }
        $this->name = $name;
        $this->age = $age;
        $this->salary = $salary;
    }
}

$john = new SecondWorker('john',25, 1000);
echo $john->age * $john->salary;